package com.farmlab.controler.scan;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.farmlab.R;

public class ScanMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_menu);
        getSupportActionBar().setTitle(" # Scan");
    }
}
