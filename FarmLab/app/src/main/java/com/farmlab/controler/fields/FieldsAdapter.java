package com.farmlab.controler.fields;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.farmlab.R;
import com.farmlab.model.fields.Field;
import java.util.ArrayList;
import java.util.List;
import android.view.ContextMenu;

/* This class is used in order to display correctly the list of fields in the fields menu.
*  With this class, only the view is loading, not all the list */
public class FieldsAdapter extends RecyclerView.Adapter<FieldsAdapter.FieldsViewHolder> {
    // Current selected field
    public static Field currentField = null;
    // Click Listener for Field Card
    private OnItemClickListener cardClickListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener) {
        cardClickListener = listener;
    }

    /* CLASS */
    // View Settings
    public class FieldsViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        private final TextView fieldName;
        private final TextView crop;
        private final TextView surface;

        public FieldsViewHolder(View itemView, final OnItemClickListener listener) {
            super(itemView);
            fieldName = itemView.findViewById(R.id.field_to_delete);
            crop = itemView.findViewById(R.id.text_view_crop);
            surface = itemView.findViewById(R.id.text_view_surface);

            // Click Listener
            itemView.setOnClickListener(v -> {
                currentField = fieldArrayList.get(getAdapterPosition());
                if (listener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(position);
                    }
                }
            });

            // Open context menu
            itemView.setOnCreateContextMenuListener(this);
        }

        // Declaration of the onCreateContextMenu
        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            currentField = fieldArrayList.get(getAdapterPosition());
        }
    }

    private List<Field> fieldArrayList = new ArrayList<>();
    private final LayoutInflater mInflater;

    FieldsAdapter(FieldsMainActivity context){ mInflater = LayoutInflater.from(context);}

    // View creation
    @Override
    public FieldsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mInflater.inflate(R.layout.fields_cards, parent, false);
        return new FieldsViewHolder(v, cardClickListener);
    }
    // Item and scrolling
    @Override
    public void onBindViewHolder(FieldsViewHolder holder, int position) {
        if (fieldArrayList != null){
            Field currentItem = fieldArrayList.get(position);
            holder.fieldName.setText(currentItem.getName());
            holder.crop.setText(currentItem.getCrop());
            holder.surface.setText(String.valueOf(currentItem.getSurface()));
        }else{
            holder.fieldName.setText("No Field");
        }
    }

    // Get the number of field in the list
    @Override
    public int getItemCount() {
        if (fieldArrayList != null)
        return fieldArrayList.size();
        else return 0;
    }

    // Set the current fieldList
    public void setFieldList(List<Field> fieldsList){
        this.fieldArrayList = fieldsList;
        notifyDataSetChanged();
    }


}
