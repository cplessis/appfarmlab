package com.farmlab.controler;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.farmlab.R;
import com.farmlab.controler.animals.AnimalsMenuActivity;
import com.farmlab.controler.calendar.CalendarMenuActivity;
import com.farmlab.controler.fields.FieldsMainActivity;
import com.farmlab.controler.scan.ScanMenuActivity;

public class MainActivity extends AppCompatActivity {

    Button fieldsMenu;
    Button animalsMenu;
    Button scanMenu;
    Button calendarMenu;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fieldsMenu = findViewById(R.id.activity_main__fields_menu);
        animalsMenu = findViewById(R.id.activity_main_animals_menu);
        scanMenu = findViewById(R.id.activity_main_scan_menu);
        calendarMenu = findViewById(R.id.activity_main_calendar_menu);

        


        fieldsMenu.setOnClickListener(v -> {
            Intent fieldsMenuActivity = new Intent(MainActivity.this, FieldsMainActivity.class);
            startActivity(fieldsMenuActivity);
        });
        animalsMenu.setOnClickListener(v -> {
            Intent animalsMenuActivity = new Intent(MainActivity.this, AnimalsMenuActivity.class);
            startActivity(animalsMenuActivity);
        });
        scanMenu.setOnClickListener(v -> {
            Intent scanMenuActivity = new Intent(MainActivity.this, ScanMenuActivity.class);
            startActivity(scanMenuActivity);
        });
        calendarMenu.setOnClickListener(v -> {
            Intent calendarMenuActivity = new Intent(MainActivity.this, CalendarMenuActivity.class);
            startActivity(calendarMenuActivity);
        });
    }
}
