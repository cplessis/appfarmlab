package com.farmlab.view.jobs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.farmlab.R;
import com.farmlab.model.jobs.Jobs;

import java.util.ArrayList;
import java.util.List;

public class JobsAdapter extends RecyclerView.Adapter<JobsAdapter.JobsViewHolder> {

    /* ATTRIBUTES */
        // Current selected job
    public static Jobs currentJobCard;
        // On card Click Listener
    private OnItemClickListener cardClickListener;
        // View Jobs List
    private List<Jobs> jobsArrayList = new ArrayList<>();
        // Inflater
    private final LayoutInflater mInflater;

    /* CLASS */
        // View settings
    public class JobsViewHolder extends RecyclerView.ViewHolder {
        private final TextView date;
        private final TextView jobType;

        public JobsViewHolder(View itemView, final OnItemClickListener listener) {
            super(itemView);
            date =itemView.findViewById(R.id.text_view_job_date);
            jobType = itemView.findViewById(R.id.text_view_job_type);

            // Click Listener
            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(position);
                    }
                }
            });
        }
    }

    /* CONSTRUCTOR */
    JobsAdapter(Context context){
        mInflater = LayoutInflater.from(context);
    }

    /* METHODS */
        // Click Listener
    private interface OnItemClickListener{
        void onItemClick(int position);
    }

    // View creation
    @Override
    public JobsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mInflater.inflate(R.layout.jobs_card, parent, false);
        return new JobsViewHolder(v, cardClickListener);
    }
    // Item and scrolling
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(JobsViewHolder holder, int position) {
        if (jobsArrayList != null){
            Jobs currentItem = jobsArrayList.get(position);
            holder.date.setText(currentItem.getJobDate());
            holder.jobType.setText(currentItem.getJobType());
        }else{
            holder.jobType.setText("No Job");
            holder.date.setText("00/00/00");
        }
    }

    /* GETTERS && SETTERS */
    @Override
    public int getItemCount() {
        if (jobsArrayList != null)
            return jobsArrayList.size();
        else return 0;
    }
    public void setJobsArrayList(List<Jobs> jobsArrayList){
        this.jobsArrayList = jobsArrayList;
        notifyDataSetChanged();
    }
}
