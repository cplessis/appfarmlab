package com.farmlab.controler.animals;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.farmlab.R;

public class AnimalsMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animals_menu);
        getSupportActionBar().setTitle(" # Animals");
    }
}
