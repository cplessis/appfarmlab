package com.farmlab.controler.jobs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.farmlab.R;
import com.farmlab.model.jobs.JobsViewModel;

public class Frag1 extends Fragment {
    private JobsViewModel jobsVM;
    private int condition;

   public Frag1(int condition){
        this.condition = condition;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // RecyclerView
        View view = inflater.inflate(R.layout.frag1, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.job_frag_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        JobsAdapter adapter = new JobsAdapter(getActivity());
        recyclerView.setAdapter(adapter);

        // Different cases for each recycler View
        switch (condition){
            case 0:
                jobsVM = new ViewModelProvider(this).get(JobsViewModel.class);
                jobsVM.getAllJobs().observe(getViewLifecycleOwner(), adapter::setJobsArrayList);
                break;

            case 1:
                jobsVM = new ViewModelProvider(this).get(JobsViewModel.class);
                jobsVM.getAllSoilPreparations().observe(getViewLifecycleOwner(), adapter::setJobsArrayList);
                break;

            case 2:
                jobsVM = new ViewModelProvider(this).get(JobsViewModel.class);
                jobsVM.getAllSowings().observe(getViewLifecycleOwner(), adapter::setJobsArrayList);
                break;

            case 3:
                jobsVM = new ViewModelProvider(this).get(JobsViewModel.class);
                jobsVM.getAllHarvest().observe(getViewLifecycleOwner(), adapter::setJobsArrayList);
                break;

            case 4:
                jobsVM = new ViewModelProvider(this).get(JobsViewModel.class);
                jobsVM.getAllTreatment().observe(getViewLifecycleOwner(), adapter::setJobsArrayList);
                break;

            case 5:
                jobsVM = new ViewModelProvider(this).get(JobsViewModel.class);
                jobsVM.getAllFertilization().observe(getViewLifecycleOwner(), adapter::setJobsArrayList);
                break;
        }
        return view;
    }
}
