package com.farmlab.controler.fields;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.example.farmlab.R;
import com.farmlab.model.fields.Field;
import com.farmlab.model.fields.FieldViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/* This class is used to creates the list of fields and manage this list*/
public class FieldsMainActivity extends AppCompatActivity {
    private FieldsAdapter mAdapter;
    private FieldViewModel fieldVM;
    public static Field SELECTED_FIELD = null;

    // ************ ON CREATE **********************************************************************
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fields_main);
        getSupportActionBar().setTitle(" # Fields ");
        registerForContextMenu(findViewById(R.id.fields_recycler_view));

        buildRecyclerView();

        FloatingActionButton fButtonAddField = findViewById(R.id.button_insert_field);
        fButtonAddField.setOnClickListener(v -> {
                    addFieldDialog();
        });

        // Save all changes in the database
        fieldVM = new ViewModelProvider(this).get(FieldViewModel.class);
        fieldVM.getAllFields().observe(this, fieldsList -> mAdapter.setFieldList(fieldsList));
    }
    //**********************************************************************************************

    // Method to build the recycler view
    public void buildRecyclerView() {
        RecyclerView mRecyclerView = findViewById(R.id.fields_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new FieldsAdapter(this);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(position -> {
            Intent infoFieldActivity = new Intent(FieldsMainActivity.this, InformationsFieldActivity.class);
            SELECTED_FIELD = mAdapter.currentField;
            startActivity(infoFieldActivity);
        });
    }

    // ADD FIELD BUTTON
    private void addFieldDialog(){
        Dialog dialog = new Dialog(FieldsMainActivity.this, R.style.Theme_AppCompat_DayNight_NoActionBar);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(100, 0, 0, 0)));
        dialog.setContentView(R.layout.dialog_add_field);
        dialog.setCancelable(true);
        dialog.show();

        Button button = dialog.findViewById(R.id.button_save_field);
        button.setOnClickListener(v -> {
            TextView editTextName = dialog.findViewById(R.id.add_name);
            TextView editTextCROP = dialog.findViewById(R.id.add_crop);
            TextView editTextSURFACE = dialog.findViewById(R.id.add_surface);

            if(TextUtils.isEmpty(editTextName.getText())
                    || TextUtils.isEmpty(editTextCROP.getText())
                    || TextUtils.isEmpty(editTextSURFACE.getText())){
                Toast.makeText(FieldsMainActivity.this, "Please insert a name, a crop and a surface", Toast.LENGTH_SHORT).show();
            }else{
                String fieldName = editTextName.getText().toString();
                String fieldCrop = editTextCROP.getText().toString();
                float fieldSurface = Float.parseFloat(editTextSURFACE.getText().toString());
                Field field = new Field(fieldName, fieldCrop, fieldSurface);
                fieldVM.insert(field);
                Toast.makeText(FieldsMainActivity.this, "Field saved", Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });
    }

    // DELETE FIELD BUTTONS
    private void deleteFieldDialog(){
        Dialog dialog = new Dialog(FieldsMainActivity.this, R.style.Theme_AppCompat_DayNight_NoActionBar);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(100, 0, 0, 0)));
        dialog.setContentView(R.layout.dialog_delete_field);
        dialog.setCancelable(true);
        dialog.show();

        Button buttonNo = dialog.findViewById(R.id.negative);
        Button buttonYes = dialog.findViewById(R.id.positive);

        TextView fieldName = dialog.findViewById(R.id.field_to_delete);
        fieldName.setText(SELECTED_FIELD.getName());

        buttonNo.setOnClickListener(v -> {
            Toast.makeText(FieldsMainActivity.this, "Field NOT deleted", Toast.LENGTH_SHORT).show();
            dialog.cancel();
        });
        buttonYes.setOnClickListener(v -> {
            fieldVM.remove(SELECTED_FIELD);
            Toast.makeText(FieldsMainActivity.this, "Field DELETED", Toast.LENGTH_SHORT).show();
            dialog.cancel();
        });
    }

    // Option menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.option_menu_fields_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.option_insert_field:
                addFieldDialog();
                return true;
            case R.id.option_share_field_database:
                Toast.makeText(this, "Fields database shared", Toast.LENGTH_SHORT);
                exportDb(this, "/data/com.example.monjardin/databases/farmLab_database" );
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    // Context menu for field Card
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.field_card_menu, menu);
    }

    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.modify_card:
                Toast.makeText(this, "Modifying options", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.copy_card:
                Toast.makeText(this, "Field duplicated", Toast.LENGTH_SHORT).show();
                Field duplicatedField = new Field(SELECTED_FIELD.getName(), SELECTED_FIELD.getCrop(), SELECTED_FIELD.getSurface());
                fieldVM.insert(duplicatedField);
                return true;
            case R.id.remove_card:
                deleteFieldDialog();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    // EXPORT DB
    public void exportDb(Context context, String dbPath) {
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        File dataDirectory = Environment.getDataDirectory();

        System.out.println("LE CHEMIN EXTERNE EST " + externalStorageDirectory);
        System.out.println("LE CHEMIN INTERNE EST " + dataDirectory);

        FileChannel source = null;
        FileChannel destination = null;

        String backupDBPath = "SampleDB.sqlite";
        File currentDB = new File(dataDirectory, dbPath);
        File backupDB = new File("/sdcard/Download/" + backupDBPath);

        try {
            source = new FileInputStream(currentDB).getChannel();
            destination = new FileOutputStream(backupDB).getChannel();
            destination.transferFrom(source, 0, source.size());

            Toast.makeText(context, "DB Exported!", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (source != null) source.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (destination != null) destination.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
