package com.farmlab.controler.calendar;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.farmlab.R;

public class CalendarMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_menu);
        getSupportActionBar().setTitle(" # Calendar");
    }
}
