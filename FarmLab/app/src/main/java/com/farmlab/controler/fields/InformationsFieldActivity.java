package com.farmlab.controler.fields;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.farmlab.R;
import com.farmlab.model.fields.Field;
import com.farmlab.model.jobs.Jobs;
import com.farmlab.model.jobs.JobsViewModel;
import com.farmlab.controler.jobs.Frag1;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

public class InformationsFieldActivity extends AppCompatActivity {
    // View Model
    JobsViewModel jobsViewModel = new JobsViewModel(getApplication());
    // Selected Field to link with the repository
    public static Field SELECTED_FIELD = FieldsMainActivity.SELECTED_FIELD;
    // Jobs List
    String[] jobTypes = {"Soil Preparation", "Sowing", "Harvest", "Treatment", "Fertilization"};


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_field_details_main);
        getSupportActionBar().setTitle(" ## Field Details");
        // Useful to update the selected field if the activity is closed and reopened
        SELECTED_FIELD = FieldsMainActivity.SELECTED_FIELD;

        // Field details initialization
        TextView name = findViewById(R.id.name_field_details);
        TextView crop = findViewById(R.id.crop_field_details);
        TextView surface = findViewById(R.id.surface_field_details);
        name.setText(SELECTED_FIELD.getName());
        crop.setText(SELECTED_FIELD.getCrop());
        surface.setText(SELECTED_FIELD.getSurface() + " ha");

        // Fragment initialization
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.activities_fragment_layout, new Frag1(0)).commit();

        //Pressing button "Add Job"
        FloatingActionButton buttonAddJob = findViewById(R.id.button_insert_job);
        buttonAddJob.setOnClickListener(v -> {
            addJobDialog();
        });

        // Tabs management
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                switch (tab.getPosition()) {
                    case 0:
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.activities_fragment_layout, new Frag1(0)).commit();
                        break;
                    case 1:
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.activities_fragment_layout, new Frag1(1)).commit();
                        break;
                    case 2:
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.activities_fragment_layout, new Frag1(2)).commit();
                        break;
                    case 3:
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.activities_fragment_layout, new Frag1(3)).commit();
                        break;
                    case 4:
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.activities_fragment_layout, new Frag1(4)).commit();
                        break;
                     case 5:
                         getSupportFragmentManager().beginTransaction()
                                 .replace(R.id.activities_fragment_layout, new Frag1(5)).commit();
                        break;
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}
            @Override
           public void onTabReselected(TabLayout.Tab tab) {}
        });
    }

    // ADD JOB DIALOG
    private void addJobDialog(){
        Dialog dialog = new Dialog(InformationsFieldActivity.this, R.style.Theme_AppCompat_DayNight_NoActionBar);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(100, 0, 0, 0)));
        dialog.setContentView(R.layout.dialog_add_job);
        dialog.setCancelable(true);
        dialog.show();


        TextView editTextName = dialog.findViewById(R.id.name_field_add_job);
        TextView editTextDate = dialog.findViewById(R.id.add_date);
        AutoCompleteTextView editTextType = dialog.findViewById(R.id.add_job_type_list);
        TextView editTextComment = dialog.findViewById(R.id.comment_add_job);

        editTextName.setText(SELECTED_FIELD.getName());
        editTextType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextType.showDropDown();
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_selectable_list_item, jobTypes);
        editTextType.setAdapter(adapter);

        Button button = dialog.findViewById(R.id.button_save_job);
        button.setOnClickListener(v -> {
            if(TextUtils.isEmpty(editTextDate.getText())
                    || TextUtils.isEmpty(editTextType.getText())
                    || TextUtils.isEmpty(editTextComment.getText())
                    || doesNotContain(editTextType.getText().toString())
            ){
                Toast.makeText(InformationsFieldActivity.this, "Please insert a valid date, job and comment", Toast.LENGTH_SHORT).show();
            }else{
                Jobs newJob = new Jobs(editTextDate.getText().toString(),
                        editTextType.getText().toString(),
                        editTextComment.getText().toString(),
                        SELECTED_FIELD.getFieldID());
                jobsViewModel.insert(newJob);
                Toast.makeText(InformationsFieldActivity.this, "Job saved", Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });
    }

    // Method Contain sequence
     private boolean doesNotContain(String inputType){
         boolean result = true;
        for (int i = 0; i < jobTypes.length && result == true; i++  ) {
            if (jobTypes[i].equals(inputType)) {
                result = false;
            }
        }
        return result;
     }
}
