package com.farmlab.model.fields;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class FieldViewModel extends AndroidViewModel {
    private FieldRepository repository;
    private LiveData<List<Field>> allFields;

    public FieldViewModel( Application application) {
        super(application);
        repository = new FieldRepository(application);
        allFields = repository.getAllFields();
    }

    public void insert(Field field){
        repository.insert(field);
    }

    public void remove(Field field){
        repository.remove(field);
    }

    public LiveData<List<Field>> getAllFields(){
        return allFields;
    }
}
