package com.farmlab.model.jobs;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class JobsViewModel extends AndroidViewModel {
    private JobsRepository repository;
    private LiveData<List<Jobs>> allJobs;
    private LiveData<List<Jobs>> allSowings;
    private LiveData<List<Jobs>> allSoilPreparations;
    public LiveData<List<Jobs>> allHarvest;
    public LiveData<List<Jobs>> allTreatment;
    public LiveData<List<Jobs>> allFertilization;


    public JobsViewModel(Application application){
        super(application);
        repository = new JobsRepository(application);
        allJobs = repository.getAllJobs();
        allSowings = repository.getAllSowings();
        allSoilPreparations = repository.getAllSoilPreparations();
        allHarvest = repository.getAllHarvest();
        allTreatment = repository.getAllTreatment();
        allFertilization = repository.getAllFertilization();
    }
    // INSERT
    public void insert(Jobs jobs){
        repository.insert(jobs);
    }
    // REMOVE
    public void remove(Jobs jobs){
        repository.remove(jobs);
    }
    // GETTER
    public LiveData<List<Jobs>> getAllJobs(){
        return allJobs;
    }
    public LiveData<List<Jobs>> getAllSoilPreparations(){
        return allSoilPreparations;
    }
    public LiveData<List<Jobs>> getAllSowings(){
        return allSowings;
    }
    public LiveData<List<Jobs>> getAllHarvest(){
        return allHarvest;
    }
    public LiveData<List<Jobs>> getAllTreatment(){
        return allTreatment;
    }
    public LiveData<List<Jobs>> getAllFertilization(){
        return allFertilization;
    }
}
