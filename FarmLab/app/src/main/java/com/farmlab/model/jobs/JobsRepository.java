package com.farmlab.model.jobs;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.farmlab.model.FarmLabDatabase;
import com.farmlab.view.fields.InformationsFieldActivity;

import java.util.List;

public class JobsRepository {
    private JobsDAO jobsDAO;
    private LiveData<List<Jobs>> allJobs;
    private LiveData<List<Jobs>> allSowings;
    private LiveData<List<Jobs>> allSoilPreparations;
    public LiveData<List<Jobs>> allHarvest;
    public LiveData<List<Jobs>> allTreatment;
    public LiveData<List<Jobs>> allFertilization;

    private int fieldID = InformationsFieldActivity.SELECTED_FIELD.getFieldID();

    public JobsRepository(Application application){
        FarmLabDatabase jobsDatabase = FarmLabDatabase.getDatabase(application);
        jobsDAO = jobsDatabase.jobsDAO();
        allJobs = jobsDAO.getAllJobs(this.fieldID);
        allSowings = jobsDAO.getAllSowings(fieldID);
        allSoilPreparations = jobsDAO.getAllSoilPreparations(fieldID);
        allHarvest =jobsDAO.getAllHarvest(fieldID);
        allTreatment = jobsDAO.getAllTreatment(fieldID);
        allFertilization = jobsDAO.getAllFertilizations(fieldID);
    }

    // INSERT
    public void insert(Jobs jobs){
        FarmLabDatabase.databaseWriterExecutor.execute(() -> {
            jobsDAO.insert(jobs);
        });
    }

    // REMOVE
    public void remove(Jobs jobs){
        FarmLabDatabase.databaseWriterExecutor.execute(() -> {
            jobsDAO.remove(jobs);
        });
    }

    // GETTER
    public LiveData<List<Jobs>> getAllJobs() {
        return allJobs;
    }
    public LiveData<List<Jobs>> getAllSoilPreparations(){
        return allSoilPreparations;
    }
    public LiveData<List<Jobs>> getAllSowings(){
        return allSowings;
    }
    public LiveData<List<Jobs>> getAllHarvest(){
        return allHarvest;
    }
    public LiveData<List<Jobs>> getAllTreatment(){
        return allTreatment;
    }
    public LiveData<List<Jobs>> getAllFertilization(){
        return allFertilization;
    }
}
