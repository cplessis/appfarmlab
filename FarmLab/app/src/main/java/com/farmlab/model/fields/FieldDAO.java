package com.farmlab.model.fields;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface FieldDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Field field);

    @Update
    void update(Field field);

    @Delete
    void remove(Field field);

    @Query("DELETE FROM fields_table")
    void deleteAllFields();

    @Query("SELECT * FROM fields_table ORDER BY name")
    LiveData<List<Field>> getAllFields();

}
