package com.farmlab.model.jobs;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface JobsDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Jobs jobs);

    @Update
    void update(Jobs jobs);

    @Delete
    void remove(Jobs jobs);

    @Query("DELETE FROM jobs_table")
    void deleteAllJobs();

    @Query("SELECT * FROM jobs_table WHERE relatedFieldID = :fieldID ORDER BY jobDate")
    LiveData<List<Jobs>> getAllJobs(int fieldID);

    @Query("SELECT * FROM jobs_table WHERE jobType = 'Soil Preparation' AND relatedFieldID = :fieldID ")
    LiveData<List<Jobs>> getAllSoilPreparations(int fieldID);

    @Query("SELECT * FROM jobs_table WHERE jobType = 'Sowing'AND relatedFieldID = :fieldID ")
    LiveData<List<Jobs>> getAllSowings(int fieldID);

    @Query("SELECT * FROM jobs_table WHERE jobType = 'Harvest' AND relatedFieldID = :fieldID ")
    LiveData<List<Jobs>> getAllHarvest(int fieldID);

    @Query("SELECT * FROM jobs_table WHERE jobType = 'Treatment' AND relatedFieldID = :fieldID ")
    LiveData<List<Jobs>> getAllTreatment(int fieldID);

    @Query("SELECT * FROM jobs_table WHERE jobType = 'Fertilization' AND relatedFieldID = :fieldID")
    LiveData<List<Jobs>> getAllFertilizations(int fieldID);
}
