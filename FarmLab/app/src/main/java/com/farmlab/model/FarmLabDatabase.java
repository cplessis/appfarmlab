package com.farmlab.model;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.farmlab.model.fields.Field;
import com.farmlab.model.fields.FieldDAO;
import com.farmlab.model.jobs.Jobs;
import com.farmlab.model.jobs.JobsDAO;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Field.class, Jobs.class}, version = 1 ,exportSchema = false) // Version of the database
public abstract class FarmLabDatabase extends RoomDatabase {
    private static volatile FarmLabDatabase instance;
    public abstract FieldDAO fieldDAO();
    public abstract JobsDAO jobsDAO();
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriterExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static synchronized FarmLabDatabase getDatabase(Context context) {
        if (instance == null) {
            synchronized (FarmLabDatabase.class){
                if (instance == null) {
                    instance = Room.databaseBuilder(context.getApplicationContext(),
                            FarmLabDatabase.class, "farmLab_database").addCallback(roomCallback).build();
                }
            }
        }
        return instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);

            databaseWriterExecutor.execute(() -> {
                FieldDAO fDao = instance.fieldDAO();
                JobsDAO jDao = instance.jobsDAO();
            });
        }
    };
}
