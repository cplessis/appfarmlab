package com.farmlab.model.jobs;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

// Table generation
@Entity(tableName = "jobs_table")
public class Jobs {

    // Table elements
    @PrimaryKey(autoGenerate = true)
    private int jobID;



    @NonNull
    private String jobDate;
    private String jobType;
    private String jobComment;
    private int relatedFieldID;

    // Constructor
    public Jobs(String date, String type, String comment, int fieldID) {
        this.jobDate = date;
        this.jobType = type;
        this.jobComment = comment;
        this.relatedFieldID = fieldID;
    }
    public Jobs(){}

    // Getters & Setters
    public void setJobID(int id) {
        this.jobID = id;
    }
    public int getJobID() {
        return jobID;
    }
    public String getJobDate() {
        return jobDate;
    }
    public String getJobType() {
        return jobType;
    }
    public String getJobComment() {
        return jobComment;
    }
    public int getRelatedFieldID() {
        return relatedFieldID;
    }
    public void setJobDate(@NonNull String jobDate) {
        this.jobDate = jobDate;
    }
    public void setJobType(String jobType) {
        this.jobType = jobType;
    }
    public void setJobComment(String jobComment) {
        this.jobComment = jobComment;
    }
    public void setRelatedFieldID(int relatedFieldID) {
        this.relatedFieldID = relatedFieldID;
    }
}

