package com.farmlab.model.fields;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

// Table generation
@Entity(tableName = "fields_table")
public class Field {

    // Table elements
    @PrimaryKey(autoGenerate = true)
    private int fieldID;

    @NonNull
    private String name;
    private String crop;
    private float surface;

    // Constructor
    public Field(String name, String crop, float surface) {
        this.name = name;
        this.crop = crop;
        this.surface = surface;
    }

    // Getters & Setters
    public void setFieldID(int id) {
        this.fieldID = id;
    }
    public int getFieldID() {
        return this.fieldID;
    }
    public String getName() {
        return name;
    }
    public String getCrop() {
        return crop;
    }
    public float getSurface() {
        return surface;
    }

}