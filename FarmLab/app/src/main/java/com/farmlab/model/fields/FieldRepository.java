package com.farmlab.model.fields;

import android.app.Application;
import androidx.lifecycle.LiveData;

import com.farmlab.model.FarmLabDatabase;

import java.util.List;

public class FieldRepository {
    private FieldDAO fieldDAO;
    private LiveData<List<Field>> allFields;

    public FieldRepository(Application application){
        FarmLabDatabase database = FarmLabDatabase.getDatabase(application);
        fieldDAO = database.fieldDAO();
        allFields = fieldDAO.getAllFields();
    }

    // INSERT
    public void insert(Field field){
        FarmLabDatabase.databaseWriterExecutor.execute(() -> {
         fieldDAO.insert(field);
        });
    }

    // REMOVE
    public void remove(Field field) {
        FarmLabDatabase.databaseWriterExecutor.execute(() -> {
            fieldDAO.remove(field);
        });
    }

    // GETTER
    public LiveData<List<Field>> getAllFields(){
        return allFields;
    }
}
