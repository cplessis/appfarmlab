# FarmLab

FarmLab is a free app developed by Clement Plessis. With FarmLab you can manage your fields. All the fields are stored in a database. 
At this time, the databse cannot be shared or synchronize to a server. This will be done soon. 
Let's download it to try !

# Application Codes

The directory "FarmLab" in the project is the complet content of the application. By opening this directory with Android Studio it is possible to modify the app. 

The [main](https://gitlab.com/cplessis/appfarmlab/-/tree/master/FarmLab/app/src/main) part of the application files 

## Java Files

The [Java files](https://gitlab.com/cplessis/appfarmlab/-/tree/master/FarmLab/app/src/main/java/com/farmlab) are the heart of the application, they are used as the connexion between all the XML files, the database, etc. 

## XML Files

The [XML files](https://gitlab.com/cplessis/appfarmlab/-/tree/master/FarmLab/app/src/main/res) compose the design part of the application ("Front End").

# Download / Install

The app can only be installed on an android system. To install an android application without the Play Store we need to install an APK file (contain the app). You can download an APK with your phone or with your computer before placing it into an accessible file of your phone (download file for example). 

- First download the "FarmLab.apk" file from the project
- Then find the APK in your downloads file
- Finally click on the APK and allow it's installation

Here is the way to install the app : 

![Installation Video](resources/appInstall.mp4)

# How to use it ?

The following video shows you the basics steps in order to discover the app and its functionnalities.

![Functions Video](resources/appFunctions.mp4)

